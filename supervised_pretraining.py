import argparse
import os

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter

import networks
import dataset_handler


class SupervisedPretraining(object):
    def __init__(self, data_path, test_set_fraction, output_dimension,
                 number_of_epochs, learning_rate, batch_size,
                 checkpoint_to_load, relative_model_path, epochs_to_save_model,
                 steps_to_evaluate_train_loss, epochs_to_evaluate_test_loss):
        # Data parameters.
        self.data_path = data_path
        self.test_set_fraction = test_set_fraction

        # Network and learning hyperparameters.
        self.output_dimension = output_dimension
        self.number_of_epochs = number_of_epochs
        self.learning_rate = learning_rate
        self.batch_size = batch_size

        # Model saving/loading parameters.
        self.checkpoint_to_load = checkpoint_to_load
        self.relative_model_path = relative_model_path
        self.epochs_to_save_model = epochs_to_save_model

        # Evaluation parameters.
        self.steps_to_evaluate_train_loss = steps_to_evaluate_train_loss
        self.epochs_to_evaluate_test_loss = epochs_to_evaluate_test_loss

        # Sets the GPU as the device if available.
        # NOTE: Both the network and the input data (x and y) must be stored in
        # the correct device using the .to() method.
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu")

        # Creates the model and the optimizer.
        self.network = networks.ConvNet(
            output_dimension=self.output_dimension).to(self.device)
        print("Currently using ", self.device, " as the device for PyTorch.")

        self.optimizer = torch.optim.Adam(self.network.parameters(),
                                          lr=self.learning_rate)

        # Loads a previous model if a checkpoint is specified.
        self.load_model()

        # Loads the relative position estimation dataset.
        self.load_dataset()

        # Initializes TensorBoard logging.
        self.tensorboard_log_path = os.path.join(self.relative_model_path,
                                                 'tensorboard_log')
        self.summary_writer = SummaryWriter(self.tensorboard_log_path)

    # Evaluates the loss over the entire test set.
    def evaluate_test_loss(self):
        average_test_loss = 0.0

        # NOTE: This step is only necessary when the model includes batch
        # normalization and/or dropout. It is crucial to set the model back to
        # the training mode before continuing training.
        self.network.eval()

        with torch.no_grad():
            number_of_batches = 0
            for sampled_batch in self.test_dataloader:
                depth_image_batch = sampled_batch['depth_image'].to(
                    self.device, non_blocking=True)
                relative_positions_batch = sampled_batch['C_r_C_O'].to(
                    self.device, non_blocking=True)

                output = self.network.forward(depth_image_batch)
                criterion = torch.nn.MSELoss()
                test_loss = criterion(output, relative_positions_batch)

                average_test_loss += test_loss.item()
                number_of_batches += 1

        # NOTE: The running test loss must be divided by the number of batches
        # because each added value represents the mean test loss over a batch.
        average_test_loss /= number_of_batches

        # Sets the model back to training mode before resuming training.
        self.network.train()

        return average_test_loss

    # Loads the dataset and creates the train and test data loaders.
    def load_dataset(self):
        pose_estimation_dataset = dataset_handler.GripperPoseEstimationDataset(
            self.data_path)

        # Randomly splits the dataset into a train and test set. (Optionally a
        # validation set as well).
        length_of_dataset = len(pose_estimation_dataset)
        self.test_set_size = int(self.test_set_fraction * length_of_dataset)
        self.train_set_size = length_of_dataset - self.test_set_size
        print('Size of the train set: %d.' % self.train_set_size)
        print('Size of the test set: %d.' % self.test_set_size)

        train_dataset, test_dataset = torch.utils.data.random_split(
            pose_estimation_dataset, [self.train_set_size, self.test_set_size])

        # Creates two separate data loaders: One for the train set and another
        # for the test set.
        # NOTE: It might be worth trying multiple workers to load the data.
        self.train_dataloader = torch.utils.data.DataLoader(
            train_dataset,
            batch_size=self.batch_size,
            shuffle=True,
            pin_memory=True)

        self.test_dataloader = torch.utils.data.DataLoader(
            test_dataset,
            batch_size=self.batch_size,
            shuffle=False,
            pin_memory=True)

    def load_model(self):
        # Loads the specified model checkpoint.
        if self.checkpoint_to_load > 0:
            full_model_path = os.path.join(
                self.relative_model_path,
                'ckpt-' + str(self.checkpoint_to_load) + '.pt')
            model_checkpoint = torch.load(full_model_path)

            self.network.load_state_dict(
                model_checkpoint['sl_network_state_dict'])
            self.optimizer.load_state_dict(
                model_checkpoint['sl_optimizer_state_dict'])

            self.epochs_so_far = model_checkpoint['total_number_of_epochs']
        else:
            self.epochs_so_far = 0

    def print_model_summary(self):
        # Prints a summary of the layers.
        print('Network: ', self.network)

        # Prints the number of parameters for each layer.
        for i, param in enumerate(self.network.parameters()):
            print('Set of parameters %d: ' % i, param.size())

        # Prints the total number of parameters in the model.
        number_of_model_parameters = sum(
            [param.nelement() for param in self.network.parameters()])
        print('Number of model parameters: ', number_of_model_parameters)

    def save_model(self, epoch):
        # Saves the model after the end of training.
        total_number_of_epochs = self.epochs_so_far + epoch + 1
        checkpoint_name = 'ckpt-' + str(total_number_of_epochs) + '.pt'
        full_model_path = os.path.join(self.relative_model_path,
                                       checkpoint_name)

        checkpoint_dict = {
            'total_number_of_epochs': total_number_of_epochs,
            'sl_network_state_dict': self.network.state_dict(),
            'sl_optimizer_state_dict': self.optimizer.state_dict(),
        }

        # NOTE: PyTorch overwrites the checkpoint if the file already exists.
        torch.save(checkpoint_dict, full_model_path)

    # The main function that implements the training loop.
    def train(self):
        number_of_update_steps = 0
        for epoch in range(self.number_of_epochs):
            average_train_loss = 0.0
            for batch_index, sampled_batch in enumerate(self.train_dataloader):
                # The sampled batch is a dictionary with the same fields that
                # are defined in the Dataset class (e.g. 'depth_image' and
                # 'C_r_C_O' in this case).

                # NOTE: The non blocking option allows the data to be
                # transferred asynchronously to the GPU (i.e. while the GPU is
                # busy training). This option can only be used when pinned
                # memory is also enabled.
                depth_image_batch = sampled_batch['depth_image'].to(
                    self.device, non_blocking=True)
                relative_positions_batch = sampled_batch['C_r_C_O'].to(
                    self.device, non_blocking=True)

                # Performs an update step.
                self.optimizer.zero_grad()
                output = self.network.forward(depth_image_batch)
                criterion = torch.nn.MSELoss()
                loss = criterion(output, relative_positions_batch)
                loss.backward()
                self.optimizer.step()
                number_of_update_steps += 1

                # Logs/Monitors the training loss every few update steps.
                average_train_loss += loss.item()
                if (batch_index % self.steps_to_evaluate_train_loss ==
                        self.steps_to_evaluate_train_loss - 1):

                    average_train_loss /= self.steps_to_evaluate_train_loss
                    self.summary_writer.add_scalar(
                        'Train/Loss',
                        average_train_loss,
                        global_step=number_of_update_steps)
                    print('[%d, %5d] Training loss: %.6f' %
                          (epoch + 1, batch_index + 1, average_train_loss))
                    average_train_loss = 0.0

            # Evaluates the test loss every few epochs.
            if (epoch % self.epochs_to_evaluate_test_loss ==
                    self.epochs_to_evaluate_test_loss - 1):
                average_test_loss = self.evaluate_test_loss()
                print('Test loss: %.6f' % average_test_loss)
                self.summary_writer.add_scalar('Test/Loss',
                                               average_test_loss,
                                               global_step=epoch)

            if (epoch %
                    self.epochs_to_save_model == self.epochs_to_save_model -
                    1):
                self.save_model(epoch)

        # Evaluates the final test loss after completing training.
        average_test_loss = self.evaluate_test_loss()
        print('Final test loss after training: %.6f' % average_test_loss)

        # Saves the model after the training run.
        self.save_model(epoch)


# Helper function to parse the script's arguments.
def parse_input_arguments():
    parser = argparse.ArgumentParser(
        description='Arguments to train a network to predict the relative'
        ' position of an object.')

    # General parameters.
    parser.add_argument('--random-seed', help='Random seed', default=974415)

    # Data parameters.
    parser.add_argument(
        '--data-path',
        help='The path to the relative position estimation dataset',
        default='/home/ramo/master_thesis/pretraining_data/baxter_step_0p02_16x16_data')
    parser.add_argument(
        '--test-set-fraction',
        help='The fraction of the data that composes the test set',
        default=0.2)

    # Network and learning hyperparameters.
    parser.add_argument('--output-dimension',
                        help='Output dimension of the network',
                        default=3)
    parser.add_argument('--number-of-epochs',
                        help='Number of epochs to train',
                        default=5)
    parser.add_argument('--lr', help='Network learning rate', default=0.001)
    parser.add_argument('--batch-size', help='Batch size', default=512)

    # Model saving/loading parameters.
    parser.add_argument('--checkpoint-to-load',
                        help='Checkpoint number to load',
                        default=0)
    parser.add_argument('--relative-model-path',
                        help='Model path relative to the current directory',
                        default='../sl_models/insert_model_name_here')

    # Evaluation parameters.
    parser.add_argument('--steps_to_evaluate_train_loss',
                        help='Number of steps to evaluate the train loss',
                        default=5)
    parser.add_argument('--epochs_to_evaluate_test_loss',
                        help='Number of epochs to evaluate the test loss',
                        default=1)
    parser.add_argument('--epochs-to-save-model',
                        help='Number of epochs to save the model',
                        default=5)

    args = vars(parser.parse_args())
    return args


if __name__ == "__main__":
    args = parse_input_arguments()
    print('Python Arguments: ', args)

    # Sets the random seeds for reproducability.
    np.random.seed(int(args['random_seed']))
    torch.manual_seed(int(args['random_seed']))

    supervised_pretraining_object = SupervisedPretraining(
        data_path=args['data_path'],
        test_set_fraction=float(args['test_set_fraction']),
        output_dimension=int(args['output_dimension']),
        number_of_epochs=int(args['number_of_epochs']),
        learning_rate=float(args['lr']),
        batch_size=int(args['batch_size']),
        checkpoint_to_load=int(args['checkpoint_to_load']),
        relative_model_path=args['relative_model_path'],
        epochs_to_save_model=int(args['epochs_to_save_model']),
        steps_to_evaluate_train_loss=int(args['steps_to_evaluate_train_loss']),
        epochs_to_evaluate_test_loss=int(args['epochs_to_evaluate_test_loss']))

    supervised_pretraining_object.train()
