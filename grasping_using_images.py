import argparse
import collections
import os
import random

import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter

from alternative_gripper import AlternativeBaxterGripper
import networks
from pyrep import PyRep
from pyrep.objects.dummy import Dummy
from pyrep.objects.shape import Shape
from pyrep.objects.vision_sensor import VisionSensor
from pyrep.robots.arms.baxter import BaxterRight


class ReacherEnv(object):
    """This class wraps the environment simulated through CoppeliaSim.
    
    The environment consists of a Baxter arm above a table. Every episode
    a basic shape is placed somewhere on the table within reach of the arm."""
    def __init__(self, scene_file_path, use_gui, number_of_continuous_actions,
                 action_movement_distance, number_of_curriculum_stages,
                 curriculum_success_rate_threshold, gripper_spawn_height_range,
                 object_workspace_length_range, object_height_threshold_range,
                 time_penalty):
        # Launches CoppeliaSim through PyRep.
        self.pr = PyRep()
        self.pr.launch(os.path.abspath(scene_file_path),
                       headless=(not use_gui))
        self.pr.start()

        # Gets the handles for the PyRep objects.
        self.robot_arm = BaxterRight()
        self.gripper = AlternativeBaxterGripper()
        self.gripper_tip = self.robot_arm.get_tip()
        self.initial_joint_positions = self.robot_arm.get_joint_positions()
        self.initial_robot_config = self.robot_arm.get_configuration_tree()
        self.initial_gripper_pose = self.gripper_tip.get_pose()
        self.previous_desired_gripper_pose = self.gripper_tip.get_pose()

        self.object_to_pick = Shape('object_to_pick')

        self.vision_sensor = VisionSensor('depth_camera')
        self.far_clipping_plane = self.vision_sensor.get_far_clipping_plane()
        # NOTE: This is the minimum depth that can be detected by a real depth
        # sensor. Since it most likely will not change, this is hardcoded here
        # for now.
        # TODO: Once the real depth sensor is known, replace this value with
        # the true minimum depth.
        self.depth_camera_minimum_depth = 0.1

        # Initializes the gripper in the open state.
        self._open_gripper()

        # Curriculum learning parameters.
        assert number_of_curriculum_stages > 1
        assert curriculum_success_rate_threshold > 0.0
        self.curriculum_stage = 0
        self.number_of_curriculum_stages = number_of_curriculum_stages
        self.curriculum_success_rate_threshold = curriculum_success_rate_threshold

        # NOTE: To fix the curriculum parameters as if there were only one
        # stage, the min and the max of the ranges have to be set to the same
        # value.

        # The range of gripper spawn heights [h_robot_min, h_robot_max].
        assert gripper_spawn_height_range[1] >= gripper_spawn_height_range[0]
        self.gripper_spawn_height_range = gripper_spawn_height_range
        # The range of the object workspace lengths [l_min, l_max].
        # NOTE: The object workspace is assumed to be square for simplicity.
        assert object_workspace_length_range[0] >= 0.0
        assert (object_workspace_length_range[1] >=
                object_workspace_length_range[0])
        self.object_workspace_length_range = object_workspace_length_range
        # The range of the height thresholds [h_obj_min, h_obj_max] to lift the
        # object above for a successful grasp.
        assert object_height_threshold_range[0] > 0.0
        assert (object_height_threshold_range[1] >=
                object_height_threshold_range[0])
        self.object_height_threshold_range = object_height_threshold_range

        # Starting values for the curriculum parameters.
        self.gripper_spawn_height = self.gripper_spawn_height_range[0]
        self.object_workspace_length = self.object_workspace_length_range[0]
        self.object_height_threshold = self.object_height_threshold_range[0]

        # The number of continuous actions and the distance to move the gripper
        # when a continuous action is taken.
        self.number_of_continuous_actions = number_of_continuous_actions
        self.action_movement_distance = action_movement_distance

        # The time penalty to incentivize the agent to complete the task as
        # quickly as possible.
        assert time_penalty >= 0
        self.time_penalty = time_penalty

        # The bounding box for the gripper tip location within the simulator.
        # NOTE: These bounds are a rough definition of the workspace. It is
        # still possible for the IK solver to fail within these bounds.
        # NOTE: This box is hardcoded for now, but only needs to be changed if
        # the simulation setup is updated.
        self.gripper_min_position_bounds = [-0.2, -0.2, 0.76]
        self.gripper_max_position_bounds = [0.2, 0.2, 1.10]

    def _clip_desired_pose_to_workspace(self, desired_pose):
        # Clips the desired position such that it always remains inside the
        # workspace.
        clipped_desired_pose = desired_pose

        # TODO: Extend this function to clip the orientation when you add the
        # rotational degree of freedom.
        for i in range(3):
            if desired_pose[i] < self.gripper_min_position_bounds[i]:
                clipped_desired_pose[i] = self.gripper_min_position_bounds[i]
            elif desired_pose[i] > self.gripper_max_position_bounds[i]:
                clipped_desired_pose[i] = self.gripper_max_position_bounds[i]

        return clipped_desired_pose

    def _close_gripper(self, velocity=0.4):
        # Closes the gripper and reports if the fingers of the gripper are
        # obstructed from reaching the target positions.
        done, obstructed = self.gripper.actuate(open_gripper=False,
                                                velocity=velocity)
        while not done:
            self.pr.step()
            done, obstructed = self.gripper.actuate(open_gripper=False,
                                                    velocity=velocity)

        if obstructed:
            print('The gripper fingers have been obstructed.')

        self.gripper_is_open = 0

    def _compute_reward(self, terminal_state):
        # NOTE: This reward shaping is a slightly modified version of the one
        # used in "3D Simulation for Robot Arm Control with Deep Q-Learning".

        if terminal_state == 1:
            # Rewards the agent for picking up the object.
            reward = 1.0
        else:
            # The agent gets a time penalty for every step to incentivize it to
            # achieve the task as quickly as possible.
            reward = -self.time_penalty

        return reward

    def _curriculum_learning_update(self, running_average_success_rate):
        # The episode_successes_buffer should be cleared every time a
        # curriculum stage is completed. This is done by setting this boolean
        # to True.
        clear_buffer = False

        if running_average_success_rate >= self.curriculum_success_rate_threshold:
            if self.curriculum_stage < self.number_of_curriculum_stages - 1:
                # Advances the curriculum to the next stage.
                self.curriculum_stage += 1
                print('Advancing to the next curriculum stage: %d' %
                      (self.curriculum_stage + 1))

                # Sets the boolean to clear the episode successes buffer.
                clear_buffer = True

            # NOTE: These values only need to be updated when the curriculum
            # advances a stage.
            self.gripper_spawn_height = (
                self.gripper_spawn_height_range[0] + self.curriculum_stage *
                (self.gripper_spawn_height_range[1] -
                 self.gripper_spawn_height_range[0]) /
                (self.number_of_curriculum_stages - 1))

            self.object_workspace_length = (
                self.object_workspace_length_range[0] + self.curriculum_stage *
                (self.object_workspace_length_range[1] -
                 self.object_workspace_length_range[0]) /
                (self.number_of_curriculum_stages - 1))

            self.object_height_threshold = (
                self.object_height_threshold_range[0] + self.curriculum_stage *
                (self.object_height_threshold_range[1] -
                 self.object_height_threshold_range[0]) /
                (self.number_of_curriculum_stages - 1))

        return clear_buffer

    # Detects if a terminal state is reached and which outcome it is.
    def _detect_terminal_state(self, grasp_success):
        if grasp_success:
            return 1
        else:
            return 0

    def _evaluate_grasp_success(self):
        """Evaluates whether the robot has successfully grasped an object and
        lifted it up.
        
        A successful grasp is defined by lifting an object over a specific
        height threshold without it falling from the gripper."""

        # NOTE: It might make sense to make the criterion depend on the gripper
        # height instead. This will be relevant if the object pose is not
        # assumed to be known.

        # TODO: Once the task is extended to pick any object from a set of
        # objects, this success criterion must be modified. It will make sense
        # to check that the gripper is obstructed and it is higher than a
        # certain height.
        _, _, object_z = self.object_to_pick.get_position()
        relative_object_height = object_z - self.initial_object_height

        # NOTE: In QT-OPT they also check that the next action moves the
        # gripper upwards. This requires more effort to implement because it
        # needs access to the DQN network.
        if relative_object_height >= self.object_height_threshold:
            return True
        else:
            return False

    def _execute_action(self, action):
        # Executes the desired action. If the action corresponds to a
        # continuous movement of the gripper, the discrete action is converted
        # to a continuous action. Otherwise, the discrete action (e.g.
        # opening/closing the gripper) is executed directly.
        if action < self.number_of_continuous_actions:
            continuous_action = self._map_discrete_to_continuous_action(action)
            # Moves the gripper (and potentially the object) according to the
            # action.
            # NOTE: The desired pose should not be updated if the current
            # action is to stay still. Otherwise the gripper would move as its
            # current position drifts due to controller errors.
            if not action == 6:
                self._move_gripper(continuous_action)

        elif action == self.number_of_continuous_actions:
            self._open_gripper()
        elif action == self.number_of_continuous_actions + 1:
            self._close_gripper()

    def _get_object_height_above_table(self, height_of_table=0.75):
        # NOTE: The height of the table is hardcoded here because it does not
        # change.
        _, _, object_z = self.object_to_pick.get_position()
        object_height_above_table = object_z - height_of_table
        return object_height_above_table

    def _get_state(self):
        # The state consists of:
        # a) a 16x16 normalized depth image, and
        # b) The binary gripper status: 0 = closed, 1 = open.

        depth_image = self.vision_sensor.capture_depth(in_meters=True)
        # Filters out any measurements that are closer than the minimum
        # detection depth of the sensor.
        depth_image[depth_image < self.depth_camera_minimum_depth] = 0.0
        normalized_depth_image = depth_image / self.far_clipping_plane

        # NOTE: In order to place both the image and the gripper status in the
        # same array, the image is flattened to a vector. The image can then be
        # unflattened as it enters the network.
        # Furthermore, the image is cast to np.float32 to save memory in the
        # replay buffer.
        normalized_depth_image = normalized_depth_image.reshape(
            (-1, )).astype(np.float32)

        # Appends the binary gripper status to the state vector.
        gripper_state = np.array([self.gripper_is_open], dtype=np.float32)
        full_state = np.concatenate((normalized_depth_image, gripper_state))
        return full_state

    def _map_discrete_to_continuous_action(self, discrete_action):
        # Transforms the discrete actions (DQN) into a simple set of
        # continuous actions (moving [up, down, etc.] a certain distance).
        # The actions are stored in List[float] to be consistent with PyRep.
        # NOTE: The actions are currently expressed in the global frame.
        if discrete_action == 0:  # Move up.
            continuous_action = [0.0, 0.0, self.action_movement_distance]
        elif discrete_action == 1:  # Move down.
            continuous_action = [0.0, 0.0, -self.action_movement_distance]
        elif discrete_action == 2:  # Move front (assume aligned with x-axis).
            continuous_action = [self.action_movement_distance, 0.0, 0.0]
        elif discrete_action == 3:  # Move back (assume aligned with x-axis).
            continuous_action = [-self.action_movement_distance, 0.0, 0.0]
        elif discrete_action == 4:  # Move left (assume aligned with y-axis).
            continuous_action = [0.0, self.action_movement_distance, 0.0]
        elif discrete_action == 5:  # Move right (assume aligned with y-axis).
            continuous_action = [0.0, -self.action_movement_distance, 0.0]

        return continuous_action

    def _move_gripper(self, continuous_action):
        # Sets the new desired pose of the gripper according to the chosen
        # action. The desired pose is computed relative to the current gripper
        # position.
        # Due to the position drifting away from the desired position, the
        # change of desired position is only computed along the intended axis
        # of motion.

        # Identifies the axis of motion from the continuous action.
        for i in range(3):
            if continuous_action[i] != 0.0:
                axis_of_motion = i
                break

        # Modifies the desired gripper position only along the axis of motion.
        # NOTE: The position of the tip dummy is what is used by the IK solver.
        current_gripper_position = self.gripper_tip.get_position()
        desired_gripper_position = self.previous_desired_gripper_pose[:3]
        desired_gripper_position[axis_of_motion] = current_gripper_position[
            axis_of_motion] + continuous_action[axis_of_motion]

        # TODO: Figure out why this part crashes the program on Leonhard.
        # desired_gripper_pose = desired_gripper_position + [0.0, 0.0, 0.0, 1.0]
        # For now, use this workaround.
        desired_gripper_pose = [None] * 7
        desired_gripper_pose[0] = desired_gripper_position[0]
        desired_gripper_pose[1] = desired_gripper_position[1]
        desired_gripper_pose[2] = desired_gripper_position[2]
        desired_gripper_pose[3] = self.previous_desired_gripper_pose[3]
        desired_gripper_pose[4] = self.previous_desired_gripper_pose[4]
        desired_gripper_pose[5] = self.previous_desired_gripper_pose[5]
        desired_gripper_pose[6] = self.previous_desired_gripper_pose[6]

        # Clips the desired gripper pose to remain inside of the workspace.
        clipped_desired_gripper_pose = self._clip_desired_pose_to_workspace(
            desired_gripper_pose)

        # Computes the target joint positions from the desired pose using IK.
        # NOTE: Since the IK solver might fail, the script should handle the
        # exception instead of crashing the program.
        # Avoids unnecessary IK computations when the desired pose does not
        # change. For example, this happens when the gripper repeatedly tries
        # moving into one of the workspace boundaries.
        if not np.allclose(np.array(clipped_desired_gripper_pose),
                           np.array(self.previous_desired_gripper_pose),
                           atol=1e-3):
            try:
                new_joint_angles = self.robot_arm.solve_ik(
                    position=clipped_desired_gripper_pose[:3],
                    quaternion=clipped_desired_gripper_pose[3:])
                self.robot_arm.set_joint_target_positions(new_joint_angles)
            except:
                print('The IK solver has failed.')

        self.previous_desired_gripper_pose = clipped_desired_gripper_pose

    def _open_gripper(self, velocity=0.4):
        # Opens the gripper.
        # NOTE: The "obstructed" boolean is only relevant when the gripper is
        # being closed.
        done, _ = self.gripper.actuate(open_gripper=True, velocity=velocity)
        while not done:
            self.pr.step()
            done, _ = self.gripper.actuate(open_gripper=True,
                                           velocity=velocity)

        self.gripper_is_open = 1

    def reset(self, running_average_success_rate):
        # Updates the curriculum parameters every time the success rate
        # threshold is exceeded.
        clear_buffer = self._curriculum_learning_update(
            running_average_success_rate)

        # Resets the robot arm and the gripper to the initial configuration.
        # TODO: Modify this later to choose a different initial state of the
        # gripper every episode.
        self.pr.set_configuration_tree(self.initial_robot_config)
        # Spawns the gripper at a specific height.
        gripper_spawn_position = [0.0, 0.0, self.gripper_spawn_height]
        gripper_spawn_joint_angles = self.robot_arm.solve_ik(
            position=gripper_spawn_position,
            quaternion=self.initial_gripper_pose[3:])
        self.robot_arm.set_joint_positions(gripper_spawn_joint_angles)
        self.robot_arm.set_joint_target_positions(gripper_spawn_joint_angles)
        self.previous_desired_gripper_pose = self.gripper_tip.get_pose()

        # Resets the gripper to the open state.
        self._open_gripper()

        # Sets the object's [x,y] position randomly with the orientation set
        # to identity within a pre-specified bounding box.
        object_xy_position = list(
            np.random.uniform([-self.object_workspace_length / 2.0] * 2,
                              [self.object_workspace_length / 2.0] * 2))
        # NOTE: This value is hardcoded because it sets the object directly on
        # the table.
        object_z_position = [0.775]
        object_position = object_xy_position + object_z_position

        identity_quaternion = [0.0, 0.0, 0.0, 1.0]
        object_pose = object_position + identity_quaternion
        self.object_to_pick.set_pose(object_pose)

        # Steps the simulation to make sure the scene is updated.
        # NOTE: If this step is not taken, the vision sensor will not correctly
        # capture the first image.
        self.pr.step()

        # Gets the initial object height to use it for grasp success evaluation.
        _, _, self.initial_object_height = self.object_to_pick.get_position()

        return self._get_state(), clear_buffer

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()

    def step(self, action, steps_to_simulate_dynamics=1):
        # Executes the action.
        self._execute_action(action)

        # NOTE: If the agent chooses a continuous action, the simulation is
        # stepped multiple times to allow the controller to get close to the
        # desired pose of the gripper.
        # NOTE: The steps_to_simulate_dynamics effectively determines the
        # "controller" rate.
        if action < self.number_of_continuous_actions:
            for _ in range(steps_to_simulate_dynamics):
                # Steps the physics simulation.
                self.pr.step()

        # Evaluates if a successful grasp has been performed.
        grasp_success = self._evaluate_grasp_success()
        # Detects if a terminal state is reached and computes the reward
        # accordingly.
        terminal_state = self._detect_terminal_state(grasp_success)
        reward = self._compute_reward(terminal_state)

        return reward, self._get_state(), terminal_state


# The DQN (Deep Q-Network) class determines how to train the above neural
# network. This algorithm only works for discrete action spaces.
class DQNAgent:
    """Basic implementation of an RL agent trained with the Deep Q-Network (DQN)
    algorithm.
    
    Includes (exponential) epsilon decay and target network updates."""
    def __init__(self,
                 state_dim,
                 action_dim,
                 number_of_actions,
                 initialize_with_sl_model,
                 freeze_sl_model_weights,
                 sl_checkpoint_to_load,
                 sl_relative_model_path,
                 rl_checkpoint_to_load,
                 rl_relative_model_path,
                 evaluation_mode=False,
                 learning_rate=0.001,
                 gamma=0.99,
                 epsilon_start=1.0,
                 epsilon_end=0.1,
                 epsilon_decay_rate=1000,
                 steps_to_update_target_network=1000):
        # NOTE: There is a distinction between the action dimensionality and
        # the number of available actions. The latter is only available for
        # discrete actions while the former applies to both continuous and
        # discrete action spaces. As such, there are two different input
        # arguments: [action_dim, number_of_actions] to reflect this difference.

        # State-action space dimensions.
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.number_of_actions = number_of_actions

        # Algorithm hyperparameters.
        self.gamma = gamma
        self.epsilon = epsilon_start  # Initialize epsilon with the start value.
        self.epsilon_start = epsilon_start
        self.epsilon_end = epsilon_end
        self.epsilon_decay_rate = epsilon_decay_rate
        self.steps_to_update_target_network = steps_to_update_target_network

        # Flags to configure the model initialization/loading.
        self.initialize_with_sl_model = initialize_with_sl_model
        self.freeze_sl_model_weights = freeze_sl_model_weights

        # Relative paths to save/load the model.
        self.sl_checkpoint_to_load = sl_checkpoint_to_load
        self.sl_relative_model_path = sl_relative_model_path
        self.rl_checkpoint_to_load = rl_checkpoint_to_load
        self.rl_relative_model_path = rl_relative_model_path

        # Enables evaluation mode to test and validate the learned agent
        # behaviour when requested.
        self.evaluation_mode = evaluation_mode

        # NOTE: The loading of the SL model is skipped if an RL checkpoint is
        # specified. This avoids loading and then overwriting the weights.
        if self.initialize_with_sl_model and not rl_checkpoint_to_load > 0:
            # Creates the Q-network and loads the pre-trained weights of a
            # supervised learning model.
            self._load_sl_model()
        else:
            # Creates a randomly initialized Q-network with the RL architecture.
            self.q_network = networks.RLConvNet(self.number_of_actions)

        # Creates the target Q-network.
        self.target_q_network = networks.RLConvNet(self.number_of_actions)

        # Enables GPU training if a GPU is available.
        self.device = torch.device(
            "cuda:0" if torch.cuda.is_available() else "cpu")
        # Sets the networks to the correct setting depending on the device.
        # NOTE: This has to happen before loading the model parameters.
        self.q_network.to(self.device)
        self.target_q_network.to(self.device)
        print("Currently using ", self.device, " as the device for PyTorch.")

        # Defines the optimizer used to update the Q-network.
        self.optimizer = torch.optim.Adam(self.q_network.parameters(),
                                          lr=learning_rate)

        # Loads the specified RL model checkpoint.
        self._load_rl_model()

        # Puts the network into evaluation mode when requested. This only
        # has an effect when using dropout or batch normalization.
        if self.evaluation_mode:
            self.q_network.eval()

        # Makes sure both networks are initialized with the same weights.
        self._update_target_network()
        # This counter increments every learning iteration and is used to
        # update algorithm parameters (e.g. target network, epsilon).
        self.count = 0

    # Calculates the loss for a particular transition.
    def _calculate_loss(self, batch_of_transitions):
        # Extracts the relevant mini-batches of states, rewards and actions.
        # Must ensure that the tensors hold correct data types (e.g. float for
        # the states).

        # Splits the state vectors into the image and the binary gripper
        # status, which is appended to the end of the vector.
        # NOTE: The images in the state need to be unflattened and reshaped to
        # the (N, C. H, W) format.
        state_batch = batch_of_transitions[:, :self.state_dim].float()
        image_batch = state_batch[:, :-1].view((-1, 1, 16, 16)).to(self.device)
        gripper_status_batch = state_batch[:, -1].view(-1, 1).to(self.device)

        action_batch = batch_of_transitions[:, self.state_dim:self.state_dim +
                                            self.action_dim].long().to(
                                                self.device)
        reward_batch = batch_of_transitions[:, self.state_dim +
                                            self.action_dim].float().to(
                                                self.device)
        terminal_state_batch = batch_of_transitions[:, self.state_dim +
                                                    self.action_dim +
                                                    1].long().to(self.device)

        next_state_batch = batch_of_transitions[:, -self.state_dim:].float()
        next_image_batch = next_state_batch[:, :-1].view(
            (-1, 1, 16, 16)).to(self.device)
        next_gripper_status_batch = next_state_batch[:, -1].view(-1, 1).to(
            self.device)

        # NOTE: DQN outputs n values corresponding to each possible action.
        # This tensor has the following shape: [batch_size, number_of_actions].
        all_state_action_values_batch = self.q_network.forward(
            image_batch, gripper_status_batch)
        # Retrieve the maximum Q(s,a) value in the current batch for logging.
        max_Q_s_a = all_state_action_values_batch.max().item()

        # This is Q(s,a), with 'a' being the action that was actually taken.
        # This tensor has the following shape: [batch_size, 1].
        taken_state_action_values_batch = torch.gather(
            all_state_action_values_batch, 1, action_batch)

        # Compute the maximum of the predicted rewards of the next state using
        # the target network.
        # NOTE: The 'torch.max' function returns a tuple of (max values, argmax
        # indices). That is why we take the 0th element.
        # This tensor has the following shape: [batch_size, 1].
        max_Q_sp1_batch = self.target_q_network.forward(
            next_image_batch,
            next_gripper_status_batch).max(dim=1, keepdim=True)[0].detach()

        # The Bellman loss is as follows:
        # 1/N * (r + gamma * max_a'(Q(next_state, a')) - Q(s,a_taken) )^2.
        # NOTE: The unsqueeze() insures that PyTorch does not incorrectly
        # broadcast the dimensions.
        expected_sum_of_rewards = reward_batch.unsqueeze(
            -1) + self.gamma * max_Q_sp1_batch

        # In the case of a positive terminal state, the expected reward
        # becomes just the reward of the current timestep.
        for i in range(terminal_state_batch.size(0)):
            # NOTE: A terminal state is a state at which the task ends. This
            # can either be a success or a failure, but it is not when the
            # episode ends because the maximum steps per episode is reached.
            if terminal_state_batch[i]:
                expected_sum_of_rewards[i] = reward_batch[i]

        loss = torch.nn.MSELoss()(expected_sum_of_rewards,
                                  taken_state_action_values_batch)
        return loss, max_Q_s_a

    def compute_discrete_action(self, state):
        # Computes the agent's action with epsilon-greedy, where epsilon is
        # the probability to choose a random action.

        # NOTE: The Q-Network is always used to compute the greedy action when
        # in evaluation mode.
        uniform_random_number = np.random.rand(1)
        if uniform_random_number > self.epsilon or self.evaluation_mode:
            with torch.no_grad():
                # Separates the state into the image and the gripper status and
                # converts them to torch tensors of appropriate dimensions.
                image = torch.from_numpy(state[:-1]).view(
                    (1, 1, 16, 16)).float().to(self.device)
                gripper_status = torch.tensor([state[-1]]).view(
                    1, 1).float().to(self.device)

                # Computes the greedy action at a given state.
                predicted_q_values = self.q_network.forward(
                    image, gripper_status)
                action = predicted_q_values.argmax().item()
        else:
            action = np.random.randint(self.number_of_actions)

        return action

    def learn(self, batch_of_transitions):
        # Updates epsilon and the target network.
        epsilon = self.update_algorithm_parameters()
        # Optimizes the Q-network with a single step and computes the loss.
        loss, max_Q_s_a = self.train_q_network(batch_of_transitions)
        return loss, max_Q_s_a, epsilon

    def _load_rl_model(self):
        if self.rl_checkpoint_to_load > 0:
            full_model_path = os.path.join(
                self.rl_relative_model_path,
                'ckpt-' + str(self.rl_checkpoint_to_load) + '.pt')
            model_checkpoint = torch.load(full_model_path)

            self.q_network.load_state_dict(
                model_checkpoint['q_network_state_dict'])
            self.optimizer.load_state_dict(
                model_checkpoint['optimizer_state_dict'])
            self.episodes_so_far = model_checkpoint['total_number_of_episodes']

            # Also loads the previous epsilon data to be consistent in
            # subsequent training runs.
            epsilon_state_dict = model_checkpoint['epsilon_state_dict']
            self.epsilon = epsilon_state_dict['epsilon']
            # The epsilon starting value is replaced with the latest epsilon
            # value for a continuous decay across training runs.
            self.epsilon_start = epsilon_state_dict['epsilon']
            self.epsilon_end = epsilon_state_dict['epsilon_end']
            self.epsilon_decay_rate = epsilon_state_dict['epsilon_decay_rate']
        else:
            self.episodes_so_far = 0

    def _load_sl_model(self):
        # Makes sure that a valid checkpoint is specified if the Q-network
        # should be initialized from a supervised learning model.
        if not self.sl_checkpoint_to_load > 0:
            raise AssertionError(
                'The checkpoint has to be larger than 0 in order to initialize'
                ' the Q-network with a pre-trained supervised learning model.')

        # NOTE: The pre-training model is different than the RL model because
        # it does not include the binary gripper status. This means that the
        # layers in common must be manually specified to transfer their weights.

        # Creates a Q-network to load the pre-trained weights into.
        self.q_network = networks.RLConvNet(self.number_of_actions)
        q_network_state_dict = self.q_network.state_dict()

        # Loads the supervised learning model checkpoint.
        full_sl_model_path = os.path.join(
            self.sl_relative_model_path,
            'ckpt-' + str(self.sl_checkpoint_to_load) + '.pt')
        sl_model_checkpoint = torch.load(full_sl_model_path)

        # Filters the manually specified layers that are in common between the
        # SL and RL models.
        # NOTE: The list of layers must be updated whenever the architecture is
        # changed.
        sl_model_state_dict = sl_model_checkpoint['sl_network_state_dict']
        layers_to_transfer = [
            'conv1.weight', 'conv1.bias', 'conv2.weight', 'conv2.bias'
        ]
        filtered_sl_model_state_dict = {
            k: v
            for k, v in sl_model_state_dict.items() if k in layers_to_transfer
        }

        # Updates and loads the state dict of the RL model to transfer the
        # weights of the common layers.
        q_network_state_dict.update(filtered_sl_model_state_dict)
        self.q_network.load_state_dict(q_network_state_dict)

        # Makes sure that the network is in the training mode.
        # NOTE: This step is only necessary when loading an SL model with
        # batch normalization and/or dropout.
        self.q_network.train()

        # Freezes all but the last fully-connected layer if requested.
        if self.freeze_sl_model_weights:
            for param in self.q_network.parameters():
                param.requires_grad = False

            # Re-creates the final layer to make it trainable.
            self.q_network.fc2 = torch.nn.Linear(256, self.number_of_actions)

    def save_model(self, current_episode):
        # Saves the model at the specified path. The total number of episodes
        # corresponds to the checkpoint number.
        # NOTE: An additional '1' is required because episodes start from 0.
        total_number_of_episodes = self.episodes_so_far + current_episode + 1
        checkpoint_name = 'ckpt-' + str(total_number_of_episodes) + '.pt'
        full_model_path = os.path.join(self.rl_relative_model_path,
                                       checkpoint_name)

        # Creates a dictionary with all epsilon decay parameters to save with
        # the model in case it is retrained later.
        # NOTE: The epsilon starting value is omitted because it is replaced
        # with the current epsilon value in subsequent training runs.
        epsilon_state_dict = {
            'epsilon': self.epsilon,
            'epsilon_end': self.epsilon_end,
            'epsilon_decay_rate': self.epsilon_decay_rate
        }

        checkpoint_dict = {
            'total_number_of_episodes': total_number_of_episodes,
            'q_network_state_dict': self.q_network.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'epsilon_state_dict': epsilon_state_dict
        }
        # NOTE: PyTorch overwrites the checkpoint if the file already exists.
        torch.save(checkpoint_dict, full_model_path)

    def train_q_network(self, batch_of_transitions):
        """Trains the Q-network for a single iteration.
        
        Args:
            batch_of_transitions: Torch tensor containing a batch of transition
                tuples.

        Returns:
            The numeric value of the loss and the maximum Q(s,a) value in the
                current batch."""
        # Sets all the gradients stored in the optimizer to zero.
        self.optimizer.zero_grad()
        # Calculates the loss for this transition.
        loss, max_Q_s_a = self._calculate_loss(batch_of_transitions)
        # Computes the gradients based on this loss, i.e. the gradients of the
        # loss with respect to the Q-network parameters.
        loss.backward()
        # Takes one gradient step to update the Q-network.
        self.optimizer.step()
        # Returns the numeric loss value and the maximum Q(s,a) value in the
        # current batch.
        return loss.item(), max_Q_s_a

    def update_algorithm_parameters(self):
        """Updates the internal algorithm parameters (target network and
        epsilon).
        
        Returns:
            The updated epsilon value."""
        # Target network update.
        if (self.count % self.steps_to_update_target_network ==
                self.steps_to_update_target_network - 1):
            self._update_target_network()
        # Epsilon update.
        self.epsilon = self.epsilon_end + (
            self.epsilon_start - self.epsilon_end) * np.exp(
                -self.count / self.epsilon_decay_rate)
        # Increments the counter every iteration and performs parameter updates
        # at a specified rate.
        self.count += 1

        return self.epsilon

    def _update_target_network(self):
        self.target_q_network.load_state_dict(self.q_network.state_dict())


class ReplayBuffer:
    """A replay buffer class that stores experiences in a deque."""
    def __init__(self, max_capacity=1000000):
        self.transitions_deque = collections.deque(maxlen=max_capacity)

    def append_transition(self, transition):
        self.transitions_deque.append(transition)

    def reset(self):
        self.transitions_deque.clear()

    def sample_transitions(self, num_samples):
        if len(self.transitions_deque) < num_samples:
            raise ValueError(
                'The replay buffer does not have enough samples yet.')
        # Sample n points uniformly at random.
        sample_transitions = collections.deque(maxlen=num_samples)
        batch_indices = np.random.randint(0, len(self.transitions_deque),
                                          num_samples)
        for i in batch_indices:
            sample_transitions.append(self.transitions_deque[i])
        if len(sample_transitions) != num_samples:
            raise ValueError(
                'Number of sampled transitions should correspond to mini-batch size.'
            )
        return sample_transitions

    def size(self):
        return len(self.transitions_deque)


def transitions_deque_to_tensor(transitions_deque, state_dim, action_dim):
    """Helper function to convert deque of transitions to torch tensor."""
    batch_size = len(transitions_deque)
    assert (batch_size > 0), 'The transitions deque is empty!'
    # The transition tuple is composed of 5 elements:
    # (state, action, reward, terminal_state, next_state).
    # Both the reward and terminal_state should always be scalars (dim = 1).
    sum_of_all_dimensions = state_dim + action_dim + 1 + 1 + state_dim
    transitions_array = np.empty((batch_size, sum_of_all_dimensions))

    for i in range(batch_size):
        transition = transitions_deque[i]
        state = transition[0]
        action = transition[1]
        reward = transition[2]
        terminal_state = transition[3]
        next_state = transition[4]

        # Assert that the tuple elements have the correct dimensions.
        # States should always be np.ndarrays.
        assert state.shape[
            0] == state_dim, 'Transition contains a state with incorrect dimensionality'
        assert next_state.shape[
            0] == state_dim, 'Transition contains a next_state with incorrect dimensionality'
        # Actions could be either a scalar (discrete case with action_dim = 1)
        # or np.ndarrays.
        if isinstance(action, int):
            assert action_dim == 1
        elif isinstance(action, np.ndarray):
            assert action.shape[
                0] == action_dim, 'Transition contains an action with incorrect dimensionality'
        else:
            raise TypeError('The action is in an unsupported format.')
        # Rewards must be floats and terminal_states must be ints.
        assert isinstance(reward, float)
        assert isinstance(terminal_state, int)

        transitions_array[i, :state_dim] = state
        transitions_array[i, state_dim:state_dim + action_dim] = action
        transitions_array[i, state_dim + action_dim] = reward
        transitions_array[i, state_dim + action_dim + 1] = terminal_state
        transitions_array[i, -state_dim:] = next_state

    transitions_tensor = torch.from_numpy(transitions_array)
    return transitions_tensor


def parse_input_arguments():
    """Helper function to parse the many input arguments of the RL setup."""
    parser = argparse.ArgumentParser(
        description=
        'Arguments to simulate and train a robot arm for manipulation.')

    # MDP parameters.
    parser.add_argument('--state-dim',
                        help='Dimension of the state',
                        default=257)
    parser.add_argument('--action-dim',
                        help='Dimension of the action',
                        default=1)
    parser.add_argument('--number-of-actions',
                        help='Number of available actions',
                        default=8)
    parser.add_argument('--number-of-continuous-actions',
                        help='Number of available continuous actions',
                        default=6)

    # Network and RL Hyperparameters.
    parser.add_argument('--number-of-episodes',
                        help='Number of episodes',
                        default=5)
    parser.add_argument('--steps-per-episode',
                        help='Number of steps per episode',
                        default=200)
    parser.add_argument('--lr', help='Network learning rate', default=0.001)
    parser.add_argument('--gamma', help='Discount factor', default=0.99)
    parser.add_argument('--batch-size', help='Batch size', default=64)
    parser.add_argument('--eps-start',
                        help='Epsilon starting value',
                        default=1.0)
    parser.add_argument('--eps-end', help='Epsilon ending value', default=0.1)
    parser.add_argument('--eps-decay-rate',
                        help='Epsilon decay rate in timesteps [t]',
                        default=10000)
    parser.add_argument('--steps-to-update-target-network',
                        help='Number of steps to update the target network',
                        default=1000)
    parser.add_argument('--buffer-size',
                        help='Maximum size of the replay buffer',
                        default=250000)
    parser.add_argument('--time-penalty',
                        help='The time penalty in the reward',
                        default=0.05)

    # Curriculum learning parameters.
    parser.add_argument('--number-of-curriculum-stages',
                        help='The number of curriculum stages',
                        default=8)
    parser.add_argument('--curriculum-success-rate-threshold',
                        help='The threshold running average success rate to'
                        ' trigger the next stage of the curriculum',
                        default=0.7)
    parser.add_argument(
        '--gripper-spawn-height-range',
        help='The range of the gripper spawn height [h_robot_min, h_robot_max]'
        ' in [m]',
        nargs='*',
        type=float,
        default=[0.82, 1.0])
    parser.add_argument(
        '--object-workspace-length-range',
        help='The range of the object workspace length [l_min, l_max] in [m]',
        nargs='*',
        type=float,
        default=[0.0, 0.2])
    parser.add_argument(
        '--object-height-threshold-range',
        help='The range of the object height threshold [h_obj_min, h_obj_max]'
        ' in [m]',
        nargs='*',
        type=float,
        default=[0.01, 0.1])

    # Simulation parameters.
    parser.add_argument('--random-seed', help='Random seed', default=974415)
    parser.add_argument('--scene-file-path',
                        help='Path to the scene file',
                        default='./full_arm_baxter.ttt')
    # Headless mode should be on by default (use_gui = False).
    parser.add_argument('--use-gui',
                        help='Flag for using the GUI',
                        action='store_true')
    parser.add_argument('--evaluation-mode',
                        help='Flag to use the evaluation mode',
                        action='store_true')
    parser.add_argument('--action-movement-distance',
                        help='Action movement distance in [m]',
                        default=0.01)

    # Model initialization and saving/loading parameters.
    parser.add_argument('--initialize-with-sl-model',
                        help='Flag to initialize the RL model with the weights'
                        ' of a trained SL model',
                        action='store_true')
    parser.add_argument('--freeze-sl-model-weights',
                        help='Flag to freeze the trained SL model weights',
                        action='store_true')
    parser.add_argument('--sl-checkpoint-to-load',
                        help='Checkpoint number to load',
                        default=0)
    parser.add_argument('--sl-relative-model-path',
                        help='Model path relative to the current directory',
                        default='../sl_models/insert_model_name_here')
    parser.add_argument('--rl-checkpoint-to-load',
                        help='Checkpoint number to load',
                        default=0)
    parser.add_argument('--rl-relative-model-path',
                        help='Model path relative to the current directory',
                        default='../models/insert_model_name_here')
    parser.add_argument('--episodes-to-save-model',
                        help='Number of episodes to save the model',
                        default=25)

    # Logging parameters.
    parser.add_argument(
        '--episodes-to-compute-average-success',
        help='Number of episodes to compute the running average success rate'
        ' during training',
        default=100)

    args = vars(parser.parse_args())
    return args


# TODO: The main function is getting crammed. Consider cleaning it up by
# dividing it into small functions.

# Main entry point
if __name__ == "__main__":
    args = parse_input_arguments()
    print('Python Arguments: ', args)

    # Sets the random seed for consistent results.
    np.random.seed(int(args['random_seed']))
    # NOTE: This one is not necessary but it is safer to just keep it.
    random.seed(int(args['random_seed']))
    torch.manual_seed(int(args['random_seed']))
    # NOTE: It turns out that the GPU operations used by CuDNN (through PyTorch)
    # add an element of randomness. Setting these flags makes the training
    # deterministic although at the cost of performance.
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False

    # Initializes the agent, the environment and the replay buffer.
    agent = DQNAgent(state_dim=int(args['state_dim']),
                     action_dim=int(args['action_dim']),
                     number_of_actions=int(args['number_of_actions']),
                     initialize_with_sl_model=args['initialize_with_sl_model'],
                     freeze_sl_model_weights=args['freeze_sl_model_weights'],
                     sl_checkpoint_to_load=int(args['sl_checkpoint_to_load']),
                     sl_relative_model_path=args['sl_relative_model_path'],
                     rl_checkpoint_to_load=int(args['rl_checkpoint_to_load']),
                     rl_relative_model_path=args['rl_relative_model_path'],
                     evaluation_mode=args['evaluation_mode'],
                     learning_rate=float(args['lr']),
                     gamma=float(args['gamma']),
                     epsilon_start=float(args['eps_start']),
                     epsilon_end=float(args['eps_end']),
                     epsilon_decay_rate=float(args['eps_decay_rate']),
                     steps_to_update_target_network=int(
                         args['steps_to_update_target_network']))

    env = ReacherEnv(
        scene_file_path=args['scene_file_path'],
        use_gui=args['use_gui'],
        number_of_continuous_actions=float(
            args['number_of_continuous_actions']),
        action_movement_distance=float(args['action_movement_distance']),
        number_of_curriculum_stages=int(args['number_of_curriculum_stages']),
        curriculum_success_rate_threshold=float(
            args['curriculum_success_rate_threshold']),
        gripper_spawn_height_range=args['gripper_spawn_height_range'],
        object_workspace_length_range=args['object_workspace_length_range'],
        object_height_threshold_range=args['object_height_threshold_range'],
        time_penalty=float(args['time_penalty']))

    replay_buffer = ReplayBuffer(int(args['buffer_size']))

    # Creates the SummaryWriter object to use TensorBoard.
    if not args['evaluation_mode']:
        tensorboard_log_path = os.path.join(args['rl_relative_model_path'],
                                            'tensorboard_log')
        summary_writer = SummaryWriter(tensorboard_log_path)

    # Creates a buffer with the episode successes (booleans) to monitor the
    # running average success rate.
    running_average_success_rate = 0.0
    episode_successes_buffer = collections.deque(
        maxlen=int(args['episodes_to_compute_average_success']))

    number_of_successful_episodes = 0
    for episode_num in range(int(args['number_of_episodes'])):

        print('Starting episode %d' % episode_num)
        state, clear_buffer = env.reset(running_average_success_rate)

        # Initializes the metrics printed at the end of every episode.
        episode_cumulative_reward = 0
        episode_average_Q_max = 0
        number_of_update_steps = 0

        for step_num in range(int(args['steps_per_episode'])):
            # The agent chooses an action according to an epsilon-greedy policy.
            action = agent.compute_discrete_action(state)

            if args['evaluation_mode']:
                print('Current action: %d' % action)

            # Steps the environment.
            reward, next_state, terminal_state = env.step(action)

            episode_cumulative_reward += reward

            # Adds the latest transition to the replay buffer.
            transition = (state, action, reward, terminal_state, next_state)
            replay_buffer.append_transition(transition)

            # Updates the current state of the simulation.
            # This is here because the env._get_state() function is not used
            # within the training loop.
            state = next_state

            # The agent starts training when there are sufficient samples in the
            # replay buffer, unless the script is in evaluation mode.
            if replay_buffer.size() >= int(
                    args['batch_size']) and not args['evaluation_mode']:
                # Samples a mini-batch of experiences.
                batch_of_transitions_deque = replay_buffer.sample_transitions(
                    int(args['batch_size']))
                batch_of_transitions_tensor = transitions_deque_to_tensor(
                    batch_of_transitions_deque, int(args['state_dim']),
                    int(args['action_dim']))
                # Performs one optimization step with the mini-batch.
                loss, max_Q_s_a, epsilon = agent.learn(
                    batch_of_transitions_tensor)

                number_of_update_steps += 1

                # Logs the loss and the max_Q_s_a of the current batch in
                # TensorBoard.
                global_step = episode_num * int(
                    args['steps_per_episode']) + step_num
                # NOTE: The loss is monitored to evaluate the effect of the
                # target network update.
                summary_writer.add_scalar('Timestep_Metrics/loss', loss,
                                          global_step)
                # Logs the epsilon value to monitor exploration.
                summary_writer.add_scalar('Epsilon/epsilon', epsilon,
                                          global_step)
                episode_average_Q_max += max_Q_s_a

            # Terminates the current episode if the task is successfully
            # completed or the maximum number of steps per episode is reached.
            # NOTE: Do not confuse episode termination conditions with terminal
            # states. The former only refers to the conditions to end an
            # episode, while the latter refers to the states that designate the
            # end of the RL task. These terminal states have a set of
            # pre-defined outcomes (e.g. success/failure) and require special
            # treatment by the RL algorithm.
            if terminal_state == 1:
                print('Successfully grasped and picked up the object. Ending'
                      ' current episode ...')
                break
            elif step_num == int(args['steps_per_episode']) - 1:
                print('The maximum number of steps per episode has been'
                      ' reached. Ending current episode ...')
                break

        # Updates the episode success buffer.
        if terminal_state:
            number_of_successful_episodes += 1
            episode_is_successful = 1
        else:
            episode_is_successful = 0

        # Clears the buffer when a curriculum stage is completed.
        if clear_buffer:
            episode_successes_buffer.clear()

        episode_successes_buffer.append(episode_is_successful)

        # Prints useful values to monitor training progress.
        if args['evaluation_mode']:
            print('Episode {:d} greedy policy reward: {:.4f}'.format(
                episode_num, episode_cumulative_reward))
        else:
            # Correctly evaluates the average Q-max value even if the batch
            # size is larger than the number of steps per episode.
            if number_of_update_steps > 0:
                episode_average_Q_max /= float(number_of_update_steps)
                print('Episode: {:d} | Reward: {:.4f}'
                      ' | Average Qmax: {:.4f}'.format(
                          episode_num + 1, episode_cumulative_reward,
                          episode_average_Q_max))

                # Logs the episode average Q_max value in TensorBoard.
                summary_writer.add_scalar(
                    'Episode_Metrics/episode_average_Q_max',
                    episode_average_Q_max,
                    global_step=episode_num + 1)
            else:
                print('Episode: {:d} | Reward: {:.4f}'.format(
                    episode_num + 1, episode_cumulative_reward))

            # Logs the average success rate over the last N episodes.
            running_average_success_rate = (
                sum(episode_successes_buffer) /
                int(args['episodes_to_compute_average_success']))
            summary_writer.add_scalar(
                'Episode_Metrics/running_average_success_rate',
                running_average_success_rate,
                global_step=episode_num + 1)

            # Saves the model every N episodes.
            if (episode_num % int(args['episodes_to_save_model']) == int(
                    args['episodes_to_save_model']) - 1):
                agent.save_model(episode_num)

    # Saves the model at the end of the training run.
    if not args['evaluation_mode']:
        agent.save_model(episode_num)
        print('Training run completed! Shutting down CoppeliaSim ...')
    else:
        print('Evaluation run completed! Shutting down CoppeliaSim ...')

    print('Number of successful episodes: %d' % number_of_successful_episodes)
    print('Success rate over all training episodes: %.4f' %
          (number_of_successful_episodes / int(args['number_of_episodes'])))
    env.shutdown()
