import errno
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import torch

from alternative_gripper import AlternativeBaxterGripper
from pyrep import PyRep
from pyrep.backend.sim import simSetDoubleSignal
from pyrep.objects.shape import Shape
from pyrep.objects.vision_sensor import VisionSensor


class DatasetGenerator(object):
    def __init__(self, scene_file_path, data_path, step_size=0.01):
        # The file paths to store the data at.
        self.data_path = data_path
        self.images_path = os.path.join(data_path, 'images')
        # TODO: Does it make more sense to be able to change this name?
        self.relative_positions_path = os.path.join(data_path,
                                                    'relative_positions.csv')

        # The step size in [m] to use when moving the gripper.
        self.step_size = step_size

        # Thhe index of the data points. This is incremented every time a new
        # data point is added to the dataset.
        self.index = 0

        # Checks that the directory does not already exist in order to prevent
        # overwriting an older dataset.
        if not os.path.exists(self.images_path):
            print('Creating a new directory for the data.')
            try:
                os.makedirs(self.images_path)
            # Guard against race condition
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        else:
            # TODO: Implement a method to extend previous datasets.
            # Perhaps I can implement this by creating a file that stores
            # metadata about the dataset. e.g. the number of existing data
            # points. Can then check that this number is consistent with the
            # number of lines in the 'positions.csv' file.
            raise NotImplementedError(
                'The specified data path has already been '
                'created. This class does not support extending '
                'previous datasets yet.')

        # Launches CoppeliaSim and handles the PyRep objects.
        self.pr = PyRep()
        self.pr.launch(scene_file_path, headless=True)
        self.pr.start()

        # Gets the handles for the PyRep objects.
        self.gripper = AlternativeBaxterGripper()
        self.initial_gripper_position = self.gripper.get_position()
        # Gets the initial gripper configuration to use it to reset the model
        # after each trajectory in case it breaks.
        self.initial_gripper_config = self.gripper.get_configuration_tree()

        self.object_to_pick = Shape('object_to_pick')

        self.vision_sensor = VisionSensor('depth_camera')
        self.far_clipping_plane_depth = self.vision_sensor.get_far_clipping_plane(
        )

        # Gets the closing joint limits to open/close the gripper by directly
        # setting the joint positions.
        self.minimum_closing_joint_limit = self.gripper.minimum_closing_joint_limit
        self.maximum_closing_joint_limit = self.gripper.maximum_closing_joint_limit

        # NOTE: To correctly open/close the gripper both the centering and
        # closing joints must be set. To center the fingers, the centering
        # joint value has to be set to: j_centering = - j_closing / 2.
        self.closed_gripper_joint_positions = [
            self.maximum_closing_joint_limit,
            -self.maximum_closing_joint_limit / 2
        ]
        self.opened_gripper_joint_positions = [
            self.minimum_closing_joint_limit,
            -self.minimum_closing_joint_limit / 2
        ]

        # Makes sure the gripper starts in the open state.
        self.gripper.set_joint_positions(self.opened_gripper_joint_positions)

    def capture_and_save_data(self, minimum_depth=0.1):
        """Captures and saves the depth image and the object's relative position
        to the camera."""
        depth_image = self.vision_sensor.capture_depth(in_meters=True)
        # Rays that intersect objects closer than the minimum detection depth
        # of the sensor are given a depth of 0.0.
        depth_image[depth_image < minimum_depth] = 0.0
        normalized_depth_image = depth_image / self.far_clipping_plane_depth

        C_r_C_O = self.object_to_pick.get_position(
            relative_to=self.vision_sensor)

        image_path = os.path.join(
            self.images_path,
            'depth_image_' + str(self.index).zfill(5) + '.npy')
        # TODO: A drawback of the .npy files is that they are not human
        # readable. Should I save them as images at the same time so I can
        # visually assess the dataset?
        np.save(image_path, normalized_depth_image)

        with open(self.relative_positions_path, 'a+') as f:
            f.write('{}, {}, {}\n'.format(C_r_C_O[0], C_r_C_O[1], C_r_C_O[2]))

        self.index += 1

    def capture_data_along_grasping_trajectory(self):
        """Moves the gripper towards the object and gathers the depth image and 
        relative position data along the trajectory.

        The gripper first moves in the x-y plane and then goes down as if it
        were to pick the object. In order to have the object visible in the
        images throughout the trajectory, the gripper stops at a certain
        height."""

        gripper_position = self.gripper.get_position()
        object_position = self.object_to_pick.get_position()

        relative_position = np.array(object_position) - np.array(
            gripper_position)
        # NOTE: The sign of the relative position is crucial to move the
        # gripper in the correct direction.
        relative_position_signs = np.sign(relative_position)
        number_of_steps_xy = np.fabs(relative_position[:2] /
                                     self.step_size).astype(np.int64)

        # Finds the dimension with the shortest displacement (between x and y
        # only).
        if relative_position[0] <= relative_position[1]:
            first_dim = 0
            second_dim = 1
        else:
            first_dim = 1
            second_dim = 0

        # Moves the gripper in the x-y plane and goes along the shorter
        # dimension first. Depth images as well as the relative positions to
        # the object (in the camera frame) are captured along the trajectory.
        for _ in range(number_of_steps_xy[first_dim]):
            gripper_position[first_dim] += (
                relative_position_signs[first_dim] * self.step_size)
            self.update_gripper_position(gripper_position)
            self.pr.step()
            self.capture_and_save_data()

        for _ in range(number_of_steps_xy[second_dim]):
            gripper_position[second_dim] += (
                relative_position_signs[second_dim] * self.step_size)
            self.update_gripper_position(gripper_position)
            self.pr.step()
            self.capture_and_save_data()

        # The displacement in z is reduced such that the gripper remains 0.25m
        # above the object at all times. This ensures that the object remains
        # visible in the depth image.
        allowed_z_displacement = np.fabs(relative_position[2]) - 0.12
        number_of_steps_z = int(allowed_z_displacement / self.step_size)

        for _ in range(number_of_steps_z):
            gripper_position[2] += (relative_position_signs[2] *
                                    self.step_size)
            self.update_gripper_position(gripper_position)
            self.pr.step()
            self.capture_and_save_data()

    def create_dataset(self, number_of_trajectories):
        # Creates a dataset by spawning the gripper and the object in different
        # configurations and then capturing data along a grasping trajectory.
        assert number_of_trajectories >= 1
        for _ in range(number_of_trajectories):
            # Spawns the object close to the gripper such that it is visible
            # in the depth images.
            # NOTE: These bounds implicitly assume that the gripper is at a
            # certain height (1.0m in this case).
            object_xy_position = list(
                np.random.uniform([-0.1, -0.15], [0.2, 0.15]))
            # NOTE: This value is hardcoded because it sets the object directly
            # on the table.
            object_z_position = [0.775]
            object_position = object_xy_position + object_z_position
            self.object_to_pick.set_position(object_position)

            # TODO: Think about spawning the cube with different orientations.
            # This would act as a form of data augmentation.
            # This will be useful when the agent is allowed to use fully 4DoF
            # actions.

            # Captures depth image and relative position data along two
            # identical trajectories with the gripper in both the open
            # and the closed state.
            self.update_gripper_position(self.initial_gripper_position)
            self.pr.set_configuration_tree(self.initial_gripper_config)
            self.gripper.set_joint_positions(self.opened_gripper_joint_positions)
            self.capture_data_along_grasping_trajectory()

            self.update_gripper_position(self.initial_gripper_position)
            self.pr.set_configuration_tree(self.initial_gripper_config)
            self.gripper.set_joint_positions(self.closed_gripper_joint_positions)
            self.capture_data_along_grasping_trajectory()

    def shutdown(self):
        self.pr.stop()
        self.pr.shutdown()

    # Sets the position of the gripper and updates the desired pose for
    # the PID controller as a way to disable it.
    def update_gripper_position(self, position):
        self.gripper.set_position(position)

        simSetDoubleSignal('x_desired', position[0])
        simSetDoubleSignal('y_desired', position[1])
        simSetDoubleSignal('z_desired', position[2])
        simSetDoubleSignal('quat_x_desired', 0.0)
        simSetDoubleSignal('quat_y_desired', 0.0)
        simSetDoubleSignal('quat_z_desired', 0.0)
        simSetDoubleSignal('quat_w_desired', 1.0)


def visualize_dataset(start_index, number_of_data_points, data_path):
    """Plots the depth images of the gathered dataset within a specified index
    range."""

    # Gets the full paths to the data.
    images_path = os.path.join(data_path, 'images')
    relative_positions_path = os.path.join(data_path, 'relative_positions.csv')

    # Reads the .csv file with all of the relative positions.
    loaded_relative_positions = np.loadtxt(relative_positions_path,
                                           delimiter=',')

    # Loads and plots the depth images in a specified index range,
    for d in range(start_index, start_index + number_of_data_points):
        image_path = os.path.join(images_path,
                                  'depth_image_' + str(d).zfill(5) + '.npy')
        depth_image = np.load(image_path)

        # NOTE: For some reason, the plotting of the images slows down over
        # time. This visualization is too slow to be practical.
        plt.imshow(depth_image)
        plt.pause(0.0001)

        # Prints the relative position to the object in the camera frame.
        print('Relative position of index %d: ' % d,
              loaded_relative_positions[d, :])


class GripperPoseEstimationDataset(torch.utils.data.Dataset):
    def __init__(self, data_path):
        self.data_path = data_path
        self.images_path = os.path.join(data_path, 'images')
        self.relative_positions_path = os.path.join(data_path,
                                                    'relative_positions.csv')
        # Reads the relative positions.
        self.relative_positions = np.loadtxt(self.relative_positions_path,
                                             dtype=float,
                                             delimiter=',')

    def __len__(self):
        # NOTE: At the moment, it is assumed that the length of the dataset
        # corresponds to the number of lines in the relative positions' file.
        # This is loosely enforced by disallowing a dataset to be extended.
        # (It does not, however, prevent a user from tampering with the file.)

        # TODO: Change the way the length of the dataset is obtained if you
        # allow extending datasets. Consider creating and using a file with
        # metadata (e.g. number of data points).
        return self.relative_positions.shape[0]

    def __getitem__(self, index):
        # Reads the depth image and the relative position (C_r_C_O) of the
        # relevant index.
        # NOTE: The DataLoader automatically converts the numpy arrays to
        # Tensors with the same dtype.
        image_path = os.path.join(
            self.images_path, 'depth_image_' + str(index).zfill(5) + '.npy')

        # NOTE: The reshape is necessary to have the image in the (C, H, W)
        # shape used by PyTorch.
        depth_image = np.load(image_path).reshape(1, 16, 16).astype(np.float32)
        C_r_C_O = self.relative_positions[index, :].astype(np.float32)

        # NOTE: This would be the place to perform any pre-processing if needed.

        sample = {'depth_image': depth_image, 'C_r_C_O': C_r_C_O}
        return sample


if __name__ == "__main__":
    SCENE_FILE = "/home/ramo/master_thesis/rl_3d_master_thesis/floating_baxter_gripper.ttt"
    DATA_PATH = '/home/ramo/master_thesis/pretraining_data/remount_step_0p02_16x16_data'
    STEP_SIZE = 0.02
    NUMBER_OF_TRAJECTORIES = 1500

    # Attempts to create a new dataset if this script is executed.
    dataset_generator = DatasetGenerator(SCENE_FILE, DATA_PATH, STEP_SIZE)

    t0 = time.time()
    dataset_generator.create_dataset(NUMBER_OF_TRAJECTORIES)
    t1 = time.time()
    print(
        'Time to generate dataset with %d trajectories and a step size '
        'of %.2f: ' % (NUMBER_OF_TRAJECTORIES, STEP_SIZE), t1 - t0)

    dataset_generator.shutdown()

    # (Optional) Visualizes the dataset.
    # visualize_dataset(0, 60, DATA_PATH)
