import numpy as np
import torch

# This is an adaptation of the CNN used in "3D Simulation for Robot Arm
# Control with Deep Q-Learning".

# Input: 16x16 depth images with 1 channel.
# Last image layer: 4x4 images with 64 channels.
# Output: N element vector. Either the actions (RL) or the relative pose (SL).

# NOTE: PyTorch takes image data with the following shape as input:
# (N, C, H, W). This represents a batch of N images with C channels, height H
# and width W.


class ConvNet(torch.nn.Module):
    """Creates a simple CNN to learn robot grasping using depth images."""
    def __init__(self, output_dimension=3):
        super(ConvNet, self).__init__()
        # Convolutional layers.
        # 5x5 convolutions with padding.
        self.conv1 = torch.nn.Conv2d(in_channels=1,
                                     out_channels=32,
                                     kernel_size=5,
                                     stride=1,
                                     padding=2)
        self.conv2 = torch.nn.Conv2d(in_channels=32,
                                     out_channels=64,
                                     kernel_size=5,
                                     stride=1,
                                     padding=2)

        # Pooling layer.
        # NOTE: The same pooling layer can be used multiple times.
        self.pool = torch.nn.MaxPool2d(2, 2)

        # Fully-connected layers.
        # NOTE: The input to the fully-connected layers should be a reshaping
        # of 4x4 images with 64 channels (64*4*4=1024).
        self.fc1 = torch.nn.Linear(64 * 4 * 4, 256)
        self.fc2 = torch.nn.Linear(256, output_dimension)

    def forward(self, x):
        x = self.pool(torch.nn.functional.relu(self.conv1(x)))
        x = self.pool(torch.nn.functional.relu(self.conv2(x)))
        x = x.view(-1, 64 * 4 * 4)
        x = torch.nn.functional.relu(self.fc1(x))
        x = self.fc2(x)
        return x


class RLConvNet(torch.nn.Module):
    """Creates a CNN that takes a depth image and the binary gripper status to
    learn robot grasping."""
    def __init__(self, output_dimension):
        super(RLConvNet, self).__init__()
        # Convolutional layers.
        # 5x5 convolutions with padding.
        self.conv1 = torch.nn.Conv2d(in_channels=1,
                                     out_channels=32,
                                     kernel_size=5,
                                     stride=1,
                                     padding=2)
        self.conv2 = torch.nn.Conv2d(in_channels=32,
                                     out_channels=64,
                                     kernel_size=5,
                                     stride=1,
                                     padding=2)
        # 3x3 convolutions with padding.
        self.conv3 = torch.nn.Conv2d(in_channels=64,
                                     out_channels=64,
                                     kernel_size=3,
                                     stride=1,
                                     padding=1)
        self.conv4 = torch.nn.Conv2d(in_channels=64,
                                     out_channels=64,
                                     kernel_size=3,
                                     stride=1,
                                     padding=1)

        # Pooling layer.
        # NOTE: The same pooling layer can be used multiple times.
        self.pool = torch.nn.MaxPool2d(2, 2)

        # This fully-connected layer is used to process the binary gripper
        # status before adding it to the image features.
        self.gripper_fc1 = torch.nn.Linear(1, 64)

        # These fully-connected layers process the final combination of the
        # image and gripper features.
        # NOTE: The input to the fully-connected layers should be a reshaping
        # of 4x4 images with 64 channels (64*4*4=1024).
        self.fc1 = torch.nn.Linear(64 * 4 * 4, 256)
        self.fc2 = torch.nn.Linear(256, output_dimension)

    def forward(self, x1, x2):
        # Processes the depth image with the first two convolutional and max
        # pool layers.
        x1 = self.pool(torch.nn.functional.relu(self.conv1(x1)))
        x1 = self.pool(torch.nn.functional.relu(self.conv2(x1)))

        # Processes the gripper status with one fully-connected layer.
        x2 = torch.nn.functional.relu(self.gripper_fc1(x2))

        # Spatially tiles the gripper features to match the (N, C, H, W) format.
        x2 = x2.view(-1, 64, 1, 1)

        # Adds the tiled gripper features to the intermediate image features
        # and passes the combined features through the last two convolutional
        # layers.
        # NOTE: Broadcasting handles this as intended.
        x = x1 + x2
        x = torch.nn.functional.relu(self.conv3(x))
        x = torch.nn.functional.relu(self.conv4(x))

        # Flattens and passes the combined features through two fully-connected
        # layers.
        x = x.view(-1, 64 * 4 * 4)
        x = torch.nn.functional.relu(self.fc1(x))
        x = self.fc2(x)
        return x


class FullyConnectedNetwork(torch.nn.Module):
    """A basic feed-forward neural network used to learn a Q-function.
    
    Args:
        input_dimension: The input dimension of the neural network.
        output_dimension: The output dimension of the neural network.
        hidden_layer_sizes: A list of integers representing the number of
            hidden units of each hidden layer."""
    def __init__(self, input_dimension, output_dimension, hidden_layer_sizes):
        # Call the initialisation function of the parent class.
        super(FullyConnectedNetwork, self).__init__()
        # Creates a fully-connected neural network with n hidden layers
        # specified by hidden_layer_sizes (n = len(hidden_layer_sizes) - 1).
        # Since an input and output layer are added, the total number of
        # layers is n + 2.
        assert len(hidden_layer_sizes) > 0

        # The input layer and the output layer need to be explicitly handled.
        self.layers = torch.nn.ModuleList()
        self.layers.append(
            torch.nn.Linear(in_features=input_dimension,
                            out_features=hidden_layer_sizes[0]))
        for i in range(len(hidden_layer_sizes) - 1):
            self.layers.append(
                torch.nn.Linear(hidden_layer_sizes[i],
                                hidden_layer_sizes[i + 1]))
        self.layers.append(
            torch.nn.Linear(in_features=hidden_layer_sizes[-1],
                            out_features=output_dimension))

    def forward(self, x):
        for i in range(len(self.layers) - 1):
            x = torch.nn.functional.relu(self.layers[i](x))
        # The final layer has no activation function (i.e. it is just a linear
        # layer).
        output = self.layers[-1](x)
        return output
