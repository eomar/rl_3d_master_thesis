from typing import List, Union

import numpy as np

from pyrep.objects.force_sensor import ForceSensor
from pyrep.objects.object import Object
from pyrep.objects.proximity_sensor import ProximitySensor
from pyrep.robots.robot_component import RobotComponent


class AlternativeGripper(RobotComponent):
    """Represents the alternative gripper model, which directly actuates one
    finger relative to the other using a closing joint. The fingers are then
    centered around the wrist with a centering joint."""
    def __init__(self, count: int, name: str, joint_names: List[str]):
        super().__init__(count, name, joint_names)
        suffix = '' if count == 0 else '#%d' % (count - 1)
        prox_name = '%s_attachProxSensor%s' % (name, suffix)
        attach_name = '%s_attachPoint%s' % (name, suffix)
        self._proximity_sensor = ProximitySensor(prox_name)
        self._attach_point = ForceSensor(attach_name)

        # NOTE: This class assumes that the gripper has two fingers/joints.
        # The first joint is the centering joint and the second is the closing
        # joint. This class only needs to access the closing joint because the
        # centering joint is handled through a child script.
        self.closing_joint = self.joints[0]
        # NOTE: The joint interval function actually returns the minimum joint
        # limit and the joint range.
        _, closing_joint_interval = self.closing_joint.get_joint_interval()
        self.minimum_closing_joint_limit = closing_joint_interval[0]
        self.maximum_closing_joint_limit = (closing_joint_interval[0] +
                                            closing_joint_interval[1])
        self.previous_position = None

    def actuate(self,
                open_gripper,
                velocity,
                threshold=0.001,
                obstruction_threshold=0.012):
        # Specifies the target velocity depending on the open/close action.
        if open_gripper:
            # NOTE: To open the joint, the joint has to move in the negative
            # direction.
            self.closing_joint.set_joint_target_velocity(-velocity)
        else:
            self.closing_joint.set_joint_target_velocity(velocity)

        done = False
        obstructed = False
        current_position = self.closing_joint.get_joint_position()
        if (self.previous_position is not None
                and np.fabs(current_position - self.previous_position) <
                threshold):
            done = True

            # Detects if an object is obstructing the gripper's fingers if the
            # gripper is being closed.
            if not open_gripper:
                # NOTE: The threshold here is larger because the fingers
                # collide before the joint can reach its maximum limit.
                if (np.fabs(current_position -
                            self.maximum_closing_joint_limit) >
                        obstruction_threshold):
                    obstructed = True

        self.previous_position = current_position

        # Resets the previous position after the open/close has been fully
        # executed.
        if done:
            self.previous_position = None

        return done, obstructed


class AlternativeBaxterGripper(AlternativeGripper):
    def __init__(self, count: int = 0):
        super().__init__(count, 'AlternativeBaxterGripper', [
            'AlternativeBaxterGripper_closingJoint',
            'AlternativeBaxterGripper_centeringJoint'
        ])
